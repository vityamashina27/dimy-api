<?php

namespace Database\Seeders;

use App\Models\Customer;
use App\Models\CustomerAddress;
use App\Models\PaymentMethod;
use App\Models\Product;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $customer = Customer::create([
            'name' => fake()->name()
        ]);

        PaymentMethod::create([
            'name' => 'GOPAY',
            'is_active' => 1
        ],[
            'name' => 'Bank Transfer',
            'is_active' => 1
        ]);

        CustomerAddress::create([
            'address' => fake()->address(),
            'customer_id' => $customer->id
        ]);

        Product::create([
            'name' => 'Indomie Ayam Bawang '.fake()->lastName(),
            'price' => '2500'
        ]);
    }
}
