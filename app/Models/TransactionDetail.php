<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class TransactionDetail extends Model
{
    use HasFactory;

    protected $fillable = ['product_id', 'qty', 'transaction_id', 'sub_total'];

    function product() : BelongsTo {
        return $this->belongsTo(Product::class);
    }
}
