<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Transaction extends Model
{
    protected $fillable = ['customer_id', 'date', 'grand_total', 'payment_id', 'customer_id', 'customer_address_id'];

    use HasFactory;

    function transaction_detail() : HasMany {
        return $this->hasMany(TransactionDetail::class)->with('product');
    }

    function customer() : BelongsTo {
        return $this->belongsTo(Customer::class);
    }

    function payment() : BelongsTo {
        return $this->belongsTo(PaymentMethod::class, 'payment_id');
    }

    function address() : BelongsTo {
        return $this->belongsTo(CustomerAddress::class, 'customer_address_id');
    }
}
