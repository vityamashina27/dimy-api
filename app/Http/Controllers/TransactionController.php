<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\CustomerAddress;
use App\Models\PaymentMethod;
use App\Models\Product;
use App\Models\Transaction;
use App\Models\TransactionDetail;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class TransactionController extends Controller
{
    function index() : JsonResponse {
        try {
            $transactions = Transaction::with('customer', 'payment', 'address', 'payment', 'transaction_detail')->get();

            return response()->json([
                'status' => true,
                'code' => 200,
                'message' => 'Success',
                'data' => $transactions
            ]);
        } catch (\Throwable $th) {
            Log::info("[GET-TRANSACTION] Error when get transaction data, [".$th->getMessage()."] ");
            Log::info('[GET-TRANSACTION] '.json_encode($th));
            return response()->json([
                'status' => false,
                'code' => 500,
                'message' => 'Failed, '.$th->getMessage(),
            ], 500);
        }
    }

    function store(Request $request) : JsonResponse {
        try {
            $request->validate([
                'customer_id' => 'required|string|max:255',
                'payment_id' => 'required|numeric',
                'customer_address_id' => 'required|numeric',
                'product_id' => 'required|numeric',
                'qty' => 'required|numeric',
            ]);

            /** Validate data */
            Customer::findOrFail($request->customer_id);
            PaymentMethod::findOrFail($request->payment_id);
            CustomerAddress::findOrFail($request->customer_address_id);
            $product = Product::findOrFail($request->product_id);

            /** Map transaction data */
            $storeTransaction = $request->only('customer_id', 'payment_id', 'customer_address_id');
            $storeTransaction['date'] = date('Y-m-d H:i:s');
            $storeTransaction['grand_total'] = $product->price * $request->qty;

            /** Store head transaction */
            $storeTransaction = Transaction::create($storeTransaction);

            /** Map transaction detail data */
            $storeTransactionDetail = $request->only('product_id', 'qty');
            $storeTransactionDetail['transaction_id'] = $storeTransaction->id;
            $storeTransactionDetail['sub_total'] = $product->price * $request->qty;

            /** Store detail transaction */
            TransactionDetail::create($storeTransactionDetail);

            return response()->json([
                'status' => true,
                'code' => 201,
                'message' => 'Created',
                'data' => Transaction::with('customer', 'payment', 'address', 'payment', 'transaction_detail')->find($storeTransaction->id)
            ]);
        } catch (\Throwable $th) {
            Log::info("[STORE-TRANSACTION] Error when store transaction data, [".$th->getMessage()."] ");
            Log::info('[STORE-TRANSACTION] '.json_encode($th));
            return response()->json([
                'status' => false,
                'code' => 500,
                'message' => 'Failed, '.$th->getMessage(),
            ], 500);
        }
    }
}
