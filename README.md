## Instalasi project
1. clone project
2. run "Composer install"
3. setup DB di env
4. run "php artisan migrate"
5. run "php artisan db:seed"
6. run "php artisan serve"

---
## Endpoint API
> - EP : [GET] http://localhost:8000/api/transaction
> - EP : [POST] http://localhost:8000/api/transaction
> - Request (body/multipart) : customer_id, payment_id, customer_address_id, product_id, qty (isi id dengan "1" semua)